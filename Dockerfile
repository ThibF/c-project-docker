FROM ruby:2.5-stretch

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
    build-essential \
    doxygen graphviz \
    plantuml \
 && rm -rf /var/lib/apt/lists/*
 
 RUN gem install ceedling
 
 WORKDIR /data
